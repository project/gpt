// Attach GPT async.
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
var gptLoader = gptLoader || 'gpt';

(function () {
  if (gptLoader !== 'gpt') return;

  var gads = document.createElement('script');
  gads.async = true;
  var useSSL = 'https:' == document.location.protocol;
  gads.src = (useSSL ? 'https:' : 'http:') +
     '//www.googletagservices.com/tag/js/gpt.js';
  var node = document.getElementsByTagName('script')[0];
  node.parentNode.insertBefore(gads, node);
})();

(function ($) {
  /**
   * Internal helper function to determine number of jQuery listeners.
   */
  function listenerCount (domEl, listener) {
    if (typeof $._data(domEl, "events")[listener] === "undefined") {
      return 0;
    }

    return $._data(domEl, "events")[listener].length;
  }
  // Keyed by slot ID, this holds size mapping definitions.
  var definedSizes = {};
  // Keyed by query param keys, this holds query param values from URL.
  var queryParamResults = {};

  Drupal.GPT = Drupal.GPT || {};
  Drupal.GPT.init = false;
  Drupal.GPT.attachHasRun = false;
  Drupal.GPT.slots = Drupal.GPT.slots || {};
  // Ad slots that have been defined but not yet displayed.
  Drupal.GPT.awaitingInit = Drupal.GPT.awaitingInit || [];
  Drupal.GPT.attachTargeting = function (slot, targeting, override) {
    var key
      , values
      , i;
    if (typeof override === 'undefined') {
      override = false;
    }
    for (key in targeting) {
      // Init values for the key.
      if (override) {
        values = [];
      }
      else {
        values = slot.getTargeting(key);
      }
      // Populate values from targeting.
      for (i = 0; i < targeting[key].length; i++) {
        if ('eval' in targeting[key][i] && Boolean(targeting[key][i].eval)) {
          try {
            values.push(eval(targeting[key][i].value));
          }
          catch (e) {
            // Silently fail bad JS.
          }
        }
        else {
          values.push(targeting[key][i].value);
        }
      }
      slot.setTargeting(key, values);
    }
  };
  Drupal.GPT.buildSizeMapping = function (sizes, slotId) {
    slotId = slotId || false;
    var i = 0;
    var m = {
      mapping: googletag.sizeMapping(),
      define: null
    };
    for (i; i < sizes.length; i++) {
      m.mapping.addSize(sizes[i][0], sizes[i][1]);
      if (sizes[i][1].length) {
        if (typeof sizes[i][1][0] === 'number') {
          m.define = sizes[i][1];
        }
        else {
          m.define = sizes[i][1][0];
        }
      }
    }

    if (slotId) {
      Drupal.GPT.sizeMappingSet(slotId, m);
    }

    return m;
  };
  Drupal.GPT.sizeMappingSet = function (slotId, mapping) {
    definedSizes[slotId] = mapping;
  }
  Drupal.GPT.sizeMappingGet = function (slotId) {
    return definedSizes.hasOwnProperty(slotId) ? definedSizes[slotId] : false;
  }

  Drupal.GPT.createSlot = function (slotId, definition, pageTargeting) {
    if (!Drupal.GPT.init) {
      Drupal.GPT.awaitingInit.push([slotId, definition, pageTargeting]);
    }
    else {
      // Add an event so third party code can react to a slot before definition.
      googletag.cmd.push(function () {
        $(document).trigger('gptSlotDefine', [slotId]);
      });
      // Pass the slotId to the definition function in the case that it must
      // be known to get the correct information.
      googletag.cmd.push(function () {
        definition(slotId);
      });
      if (pageTargeting) {
        googletag.cmd.push(function () {
          Drupal.GPT.attachTargeting(
              Drupal.GPT.grabSlot(slotId),
              Drupal.settings.gpt.targeting);
        });
      }
      // Add an event so third party code can react to a slot definition before
      // ad display.
      googletag.cmd.push(function () {
        $(document).trigger('gptSlotDefined', [slotId]);
      });

      Drupal.GPT.displaySlot(slotId);
    }
  };
  Drupal.GPT.displaySlot = function (slotId) {
    googletag.cmd.push(function () {
      // If slot is not created, do not try to display it.
      var slot = Drupal.GPT.grabSlot(slotId);
      if (slot === false) {
        return;
      }
      googletag.display(slotId);
      Drupal.GPT.refreshSlots([slot]);
    });
  };
  /**
   * Retrieve one slot object, if slotId, provided or an object of all slots.
   */
  Drupal.GPT.grabSlot = function (slotId) {
    if (typeof slotId === 'undefined') {
      return Drupal.GPT.slots;
    }
    if (typeof this.slots[slotId] !== 'undefined') {
      return Drupal.GPT.slots[slotId];
    }
    return false;
  };
  /**
   * Refreshes list of slots, but updates correlator once.
   *
   * For GPT to see these ad refreshes as companion, the correlator value must
   * be updated once, and then all ads can be refreshed.
   *
   * @param slots
   *   If provided, should be an array of ad definitions.
   */
  Drupal.GPT.refreshSlots = function (slots) {
    if (typeof slots === 'undefined') {
      slots = null;
    }
    // Miliseconds to wait for third parties to finish their work.
    var maxDelay = 500;

    // Note debouncing may be desired here.
    googletag.cmd.push(function () {
      Drupal.GPT.wait(maxDelay, document, 'gptSlotsRefresh', [slots], function () {
        googletag.pubads().updateCorrelator();
        googletag.pubads().refresh(slots, {changeCorrelator: false});
      });
    });
  }
  /**
   * Helper to update the correlator value for GPT ads.
   *
   * After this is called, any calls to googletag.pubads().refresh() should
   * pass parameter to not refresh correlator.
   */
  Drupal.GPT.updateCorrelator = function () {
    googletag.cmd.push(function() {
      googletag.pubads().updateCorrelator();
    });
  }
  Drupal.GPT.unitPath = function () {
    var p = [Drupal.settings.gpt.networkCode];
    if ('targetedUnit' in Drupal.settings.gpt
        && Drupal.settings.gpt.targetedUnit.length) {
      p.push(Drupal.settings.gpt.targetedUnit);
    }
    return '/' + p.join('/');
  }
  // Simple promise-esque handle which provides timeout-able execution with an
  // unknown list of handlers.
  Drupal.GPT.wait = function (timeout, domEl, eventName, args, action) {
    var count = listenerCount(domEl, eventName);
    // If no listeners are bound, execute the action now.
    if (count === 0) {
      action();
      return;
    }

    // Set a timeout which upon completion will execute the action and prevent
    // the done callback from executing the action.
    var delay = setTimeout(function () {
      count = 0;
      action();
    }, timeout);

    // Create a done function which will execute the action when all listeners
    // have responded and clear the timeout.
    var done = function () {
      count--;
      if (count === 0) {
        clearTimeout(delay);
        action();
      }
    }

    // Prepend the args with done so that listeners can respond.
    args.unshift(done);

    $(domEl).trigger(eventName, args);
  }

  /**
   * Fetches a key's value from URL query parameters.
   *
   * @param {string} key
   *   The key of whose value to return. Only the first instance found will be
   *   recognized.
   * @param {bool} reset
   *   If true, the static cache of found values is reset. Defaults to false.
   *   Should be used if the URL's query parameters change dynamically.
   * @return {string}
   *   The found value. If nothing found, an empty string is returned.
   */
  Drupal.GPT.getQueryParamValue = function (key, reset) {
    reset = reset || false;
    if (reset) {
      queryParamResults = {};
    }

    if (typeof queryParamResults[key] === 'undefined') {
      var result = '';
      var re = new RegExp("[?&]" + key + "=([^&#]*)", 'i');
      var matches = re.exec(window.location.search);
      if (matches && typeof matches[1] === 'string' && matches[1] !== '') {
        result = decodeURIComponent(matches[1]);
      }
      queryParamResults[key] = result;
    }

    return queryParamResults[key];
  }

  Drupal.behaviors.gpt = {
    attach: function (context, settings) {
      if (Drupal.GPT.attachHasRun) return;
      Drupal.GPT.attachHasRun = true;

      googletag.cmd.push(function () {
        Drupal.GPT.init = true;

        // Override googletag.defineSlot so that we can store references to the
        // slots defined, since Google doesn't provide a documented method to
        // retrieve defined slots.
        var gptDefineSlot = googletag.defineSlot;
        googletag.defineSlot = function (adUnitPath, size, div) {
          Drupal.GPT.slots[div] = gptDefineSlot(adUnitPath, size, div);
          return Drupal.GPT.slots[div];
        };

        // Override googletag.defineOutOfPageSlot for same reason as above.
        var gptDefineOutOfPageSlot = googletag.defineOutOfPageSlot;
        googletag.defineOutOfPageSlot = function (adUnitPath, div) {
          Drupal.GPT.slots[div] = gptDefineOutOfPageSlot(adUnitPath, div);
          return Drupal.GPT.slots[div];
        };

        // Collapse empty divs.
        if (settings.gpt.collapseEmptyDivs) {
          googletag.pubads().collapseEmptyDivs();
        }

        // Enable SRA while disabling initial load. Must use refresh() to
        // fetch newly created ads. This gives us the most flexibility.
        googletag.pubads().enableSingleRequest();
        googletag.pubads().disableInitialLoad();

        // Enable services.
        googletag.enableServices();

        // Create slots that have been waiting.
        var awaitingInit = Drupal.GPT.awaitingInit;
        for (var i = 0; i < awaitingInit.length; i++) {
          Drupal.GPT.createSlot(awaitingInit[i][0], awaitingInit[i][1], awaitingInit[i][2]);
        }
      });
    }
  };
})(jQuery);
